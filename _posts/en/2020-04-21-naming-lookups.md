---
title: "Naming lookups"
lang: en
ref: workflows
categories: [Salesforce]
tags: [Best Practices, The Elements of Salesforce]
excerpt: "Do you think that naming a lookup is easy? Think again."
---

This is the third article in "The elements of Salesforce", a running series of posts named after [Strunk and White's style guide](https://en.wikipedia.org/wiki/The_Elements_of_Style). In these articles, I try to describe the practices that I find myself converging towards. I update them as appropriate. Feel free to send over any feedback or ideas.

Almost every metadata component in Salesforce has two names, the external, user-facing one, also called "label", and the internal one, to be used by code and APIs, which is also called "API name" or, at times, just "name". 

In this article we will take a look at a what happens in terms of naming when a lookup relationship is created.

## Lookups

At first it is hard to realize that when we create every lookup, we define four different names: a label and an API name for the reference from the child record to the parent, as we would expect from any field, and a label and an API name for the related list. The former are quite straightforward, but the latter are a bit more nuanced and easier to miss. And this can lead to chaos when we have multiple lookups to the same object.

We can see how it works with a minimalistic example. Suppose that we have a custom object called Widget (API name: `Widget__c`). Three people will intervene in the production of a widget: one will collect it, one will bake it in the furnace and one will package it to be sent to a wholesaler. These roles change from widget to widget and we want to capture them in Salesforce. To that end, we create three fields in Widget: three lookups to Contact named Collector (`Collector__c`), Baker (`Baker__c`) and Packager (`Packager__c`).

Now suppose that we have created these fields with all the default names proposed by Salesforce. This means that the related list corresponding to Collector will be named in the field parameter called "Related List Label" by taking the plural form of the name of the child object. You guessed right: "Widgets". Now, what about Baker? Well... er..."Widgets". And Packager? Yes, also "Widgets". Suppose you go to check on contact [Dale B. Cooper](https://en.wikipedia.org/wiki/Dale_Cooper). In his page you will see three related lists named "Widgets". It will be hard to figure out which are the widgets he has worked on as a collector, as a baker, or as a packager. On the API name side of things, the related list will be called `Widgets__r`, `Widgets1__r` and `Widgets2__r`. Not very descriptive either, right?

Now, let's rewind to the point where we created the lookups, but let's make sure that we are entering the right values in Child Relationship Name and Related List Label:

Field Label | Field Name | Child Relationship Name | Related List Label
---|---|---|---
Collector | `Collector__c` | `WidgetsAsCollector__r` | Widgets (Collector)
Baker | `Baker__c` | `WidgetsAsBaker__r` | Widgets (Baker)
Packager | `Packager__c` | `WidgetsAsPackager__c` | Widgets (Packager)

This will solve the issue by identifying each related list properly and without ambiguity.

And, with a clearer understanding of the issue, here are the general rules:

* Label and API name should follow the same criteria for naming described above.
* Both the related list's API name and its label are defaulted to the plural of the child object. If we know or suspect that there will be more than one lookup from the same child to the same parent, these defaults must change as follows; otherwise the following changes are optional:
  * API name ("Child Relationship Name"): `[plural of the child object]` + `As` + `[label of the field]` + `__r` (replace `As` with any preposition that makes sense in the context)
  * Label ("Related List Label"): The plural of the child object plus the label of the field in parentheses.

### Revision history

2020-04-21 --- Published
