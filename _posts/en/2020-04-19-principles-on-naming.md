---
title: "Principles on naming"
lang: en
ref: workflows
categories: [Salesforce]
tags: [Best Practices, The Elements of Salesforce]
excerpt: "These are some common naming practices that can make development and configuration more sustainable."
---

This is the first article in "The elements of Salesforce", a running series of posts named after [Strunk and White's style guide](https://en.wikipedia.org/wiki/The_Elements_of_Style). In these articles, I try to describe the practices that I find myself converging towards. I update them as appropriate. Feel free to send over any feedback or ideas.

Almost every metadata component in Salesforce has two names, the external, user-facing one, also called "label", and the internal one, to be used by code and APIs, which is also called "API name" or, at times, just "name". 

In this article we will take a look at a few common naming practices that we can adopt to make development and configuration more sustainable.

## Be consistent

If today we take home just one piece of advice, let's make it this one: be consistent. Do not mix British and American English in field names. Stick to one capitalization criteria. Use our common sense. Help our users by spreading a unified sense of style to all the corners of the application, without stridencies.

Two [corollaries](https://en.wikipedia.org/wiki/Corollary) to the above:

* Help others be consistent by documenting our decisions in a style guide
* When we define our criteria, consider Salesforce's own choices for all standard metadata components as a general guideline

## No abbreviations, no overrides

Whenever possible, use full words:

* In labels, use abbreviations only when **all the end users** in the org will understand their meaning, and even then, make sure there is enough context provided in the help text.
* In API names, use abbreviations only when there is enough objective documentation (e.g. textbooks, laws) on what an abbreviation means.

In case of doubt, remember these two rules of thumb:

1. Labels and API names should be as similar as possible.
2. Always think about our audience: labels are chosen for end users; API names are chosen for all developers, programmatic or declarative. Developers may lack the degree of domain knowledge that we can expect from an end user.

As a corolary to the rules of thumb above, we will try to avoid by all means necessary changing the names of existing metadata via translation workbench or translation override.

## No underscores in API names

For many metadata components, and for objects and fields in particular, the system automatically proposes for the API name a string equal to the label, replacing spaces with underscores. We will *almost always* ignore this proposal. Don't fret. Salesforce [ignores it too](https://slalom.quip.com/MW5cAPVwat8k/Salesforce-Naming-Conventions).

Instead, we will do the following:

* Start with the label
* Remove all diacritic accents, tildes and umlauts, as well as all self-standing symbols (slashes, dashes, etc)
* Capitalize only the first letter in every word. It does not matter what type of word it is. This includes initializations.
* Remove all spaces. Do not use underscores to separate the words in the label.

DO | DON'T
--- | ---
`FavoriteOecdCountryInTheWorld__c` | `Favorite_OECD_Country_in_the_World_c`<br/>`FavoriteOECDCountryInTheWorld__c`<br/>`FavoriteOECDCountryintheWorld__c`

Why do this?

* **Compactness.** It takes less characters, which we can then use to add meaning and structure to our metadata (e.g. see below regarding prefixes and logical groupings).
* **Consistency.** It parallels Salesforce's own naming convention for standard objects and fields. Salesforce's own Success Cloud team [follows a similar approach](https://trailhead.salesforce.com/content/learn/modules/success-cloud-coding-conventions/choose-naming-conventions-sc).

## Group components logically

Optionally, prefix every API name with a 2--4 character sequence followed by an underscore, to denote what application or subsystem within the org a component belongs to. This is a governance tool, especially useful in orgs in which multiple independent applications are hosted.

Another possibility would be to group components at the sub-package level. For instance, a bank could prefix all the custom fields at the Account level that have to do with the [Know Your Customer process](https://en.wikipedia.org/wiki/Know_your_customer) with `kyc_`, so that all the fields can be found together by sorting by API name.

Note that if [unlocked packages](https://trailhead.salesforce.com/en/content/learn/modules/sfdx_dev_model) are used for development, this convention can be ignored at the application level --- if both were used, source of truth problems could arise easily, as it is easier to move components across packages than it is to rename a component.

### Revision history

2020-04-19 --- Published
